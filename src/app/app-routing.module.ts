import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [RouterModule.forRoot([
    {
      path: '',
      pathMatch: 'full',
      redirectTo: 'auth',
    },
    {
      path: 'foo',
      loadChildren: () => {
        return import('./modules/foo/foo.module').then((m) => m.FooModule);
      },
    },
    {
      path: 'bar',
      loadChildren: () => {
        return import('./modules/bar/bar.module').then((m) => m.BarModule);
      },
    },
  ])],
  exports: [RouterModule],
})
export class AppRoutingModule {
}
