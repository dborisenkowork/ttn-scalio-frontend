import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.pug',
  styleUrls: [
    'app.component.scss',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent {
}
