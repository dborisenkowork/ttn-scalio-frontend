import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  templateUrl: 'foo.component.pug',
  styleUrls: [
    'foo.component.scss',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class FooComponent
{}
