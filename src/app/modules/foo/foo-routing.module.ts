import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FooComponent } from './components/foo/foo.component';
import { AreUserCredentialsVerifiedGuard } from '@app/modules/shared/guards/are-user-credentials-verified.guard';

@NgModule({
  imports: [RouterModule.forChild([
    {
      path: '',
      pathMatch: 'full',
      component: FooComponent,
      canActivate: [
        AreUserCredentialsVerifiedGuard,
      ],
    },
  ])],
  exports: [RouterModule],
})
export class FooRoutingModule
{}
