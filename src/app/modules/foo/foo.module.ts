import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { FooComponent } from './components/foo/foo.component';
import { FooRoutingModule } from './foo-routing.module';

@NgModule({
  imports: [
    SharedModule,
    FooRoutingModule,
  ],
  declarations: [
    FooComponent,
  ],
})
export class FooModule
{}
