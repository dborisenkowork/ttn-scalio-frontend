import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

export interface AccountSignUpRequest {
  username: string;
  password: string;
}

export interface AccountSignUpResponse {
  success: true;
}

@Injectable()
export class AccountTtnApiService
{
  constructor(
    private readonly http: HttpClient,
  ) {}

  signUp(request: AccountSignUpRequest): Observable<AccountSignUpResponse> {
    return this.http.post<AccountSignUpResponse>(`${environment.ttnApiEndpoint}/signup`, request);
  }
}
