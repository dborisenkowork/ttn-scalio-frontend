import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from '../../../../environments/environment';

export interface AuthLoginRequest {
  username: string;
  password: string;
}

export interface AuthLoginResponse {
  success: true;
}

@Injectable()
export class AuthTtnApiService
{
  constructor(
    private readonly http: HttpClient,
  ) {}

  login(request: AuthLoginRequest): Observable<AuthLoginResponse> {
    return this.http.post<AuthLoginResponse>(`${environment.ttnApiEndpoint}/login`, request);
  }
}
