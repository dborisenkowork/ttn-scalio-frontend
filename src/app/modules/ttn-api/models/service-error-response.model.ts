export class ServiceErrorResponse {
  success: false;
  status: number;
  message?: string;
}
