import { NgModule } from '@angular/core';
import { AccountTtnApiService } from './services/account.ttn-api-service';
import { AuthTtnApiService } from './services/auth.ttn-api-service';
import { HttpClientModule } from '@angular/common/http';

const services = [
  AccountTtnApiService,
  AuthTtnApiService,
];

@NgModule({
  imports: [
    HttpClientModule,
  ],
  providers: [
    ...services,
  ],
})
export class TtnApiModule
{}
