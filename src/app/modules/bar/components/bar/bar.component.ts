import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  templateUrl: 'bar.component.pug',
  styleUrls: [
    'bar.component.scss',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BarComponent
{}
