import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BarComponent } from './components/bar/bar.component';
import { AreUserCredentialsVerifiedGuard } from '@app/modules/shared/guards/are-user-credentials-verified.guard';

@NgModule({
  imports: [RouterModule.forChild([
    {
      path: '',
      pathMatch: 'full',
      component: BarComponent,
      canActivate: [
        AreUserCredentialsVerifiedGuard,
      ],
    },
  ])],
  exports: [RouterModule],
})
export class BarRoutingModule
{}
