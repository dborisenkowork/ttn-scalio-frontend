import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { BarComponent } from './components/bar/bar.component';
import { BarRoutingModule } from './bar-routing.module';

@NgModule({
  imports: [
    SharedModule,
    BarRoutingModule,
  ],
  declarations: [
    BarComponent,
  ],
})
export class BarModule
{}
