import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable, of } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { UserCredentialsService } from '@app/modules/shared/services/user-credentials.service';
import { AuthTtnApiService } from '@app/modules/ttn-api/services/auth.ttn-api-service';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable()
export class AreUserCredentialsVerifiedGuard implements CanActivate {
  constructor(
    private readonly router: Router,
    private readonly matSnackbar: MatSnackBar,
    private readonly userCredentialsService: UserCredentialsService,
    private readonly authTtnApiService: AuthTtnApiService,
  ) {}

  // @ts-ignore
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | UrlTree | boolean {
    const userCredentials = this.userCredentialsService.userCredentials;

    if (userCredentials) {
      if (this.userCredentialsService.isVerified) {
        return true;
      } else {
        return this.authTtnApiService.login({
          username: userCredentials.username,
          password: userCredentials.password,
        }).pipe(
          map(() => true),
          tap(() => {
            this.userCredentialsService.markAsVerified();
          }),
          catchError(() => {
            return of(this.failInvalidCredentials());
          }),
        );
      }
    } else {
      this.failNoCredentials();
    }
  }

  failNoCredentials(): UrlTree {
    this.matSnackbar.open('Enter your credentials.');

    return this.router.parseUrl('/');
  }

  failInvalidCredentials(): UrlTree {
    this.matSnackbar.open('Your credentials are invalid.');

    return this.router.parseUrl('/');
  }
}
