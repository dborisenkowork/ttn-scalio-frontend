import { NgModule } from '@angular/core';
import { SharedMaterialModule } from './shared-material.module';
import { TtnApiModule } from '../ttn-api/ttn-api.module';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AreUserCredentialsVerifiedGuard } from '@app/modules/shared/guards/are-user-credentials-verified.guard';

const modules: Array<any> = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  SharedMaterialModule,
  TtnApiModule,
];

const guards: Array<any> = [
  AreUserCredentialsVerifiedGuard,
];

@NgModule({
  imports: [
    ...modules,
  ],
  providers: [
    ...guards,
  ],
  exports: [
    ...modules,
  ],
})
export class SharedModule
{}
