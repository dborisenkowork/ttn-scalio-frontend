import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

export interface UserCredentials {
  username: string;
  password: string;
}

type Current = UserCredentials | undefined;

@Injectable({
  providedIn: 'root',
})
export class UserCredentialsService
{
  private _isVerified: boolean = false;
  private _userCredentials$: BehaviorSubject<Current> = new BehaviorSubject<Current>(undefined);

  get userCredentials$(): Observable<Current> {
    return this._userCredentials$.asObservable();
  }

  get userCredentials(): Current {
    return this._userCredentials$.getValue();
  }

  setUserCredentials(userCredentials: Current): void {
    this._userCredentials$.next(userCredentials);
    this._isVerified = false;
  }

  markAsVerified(): void {
    if (this.userCredentials) {
      this._isVerified = true;
    }
  }

  get isVerified(): boolean {
    return this._isVerified;
  }
}

