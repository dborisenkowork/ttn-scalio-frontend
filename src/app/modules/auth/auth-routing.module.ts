import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AuthComponent } from './components/auth/auth.component';

@NgModule({
  imports: [RouterModule.forChild([
    {
      path: 'auth',
      pathMatch: 'full',
      component: AuthComponent,
    },
  ])],
  exports: [RouterModule],
})
export class AuthRoutingModule
{}
