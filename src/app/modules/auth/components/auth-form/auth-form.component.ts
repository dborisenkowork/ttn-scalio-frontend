import { AfterViewInit, ChangeDetectionStrategy, Component, ElementRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { accountMaxPasswordLength, accountMaxUsernameLength, accountMinPasswordLength, accountMinUsernameLength } from '@app/validation/account.validation';
import { MatSnackBar } from '@angular/material';
import { UserCredentialsService } from '@app/modules/shared/services/user-credentials.service';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

interface FormValue {
  username?: string;
  password?: string;
}

interface State {
  form: FormGroup;
  accountMinUsernameLength: number;
  accountMaxUsernameLength: number;
  accountMinPasswordLength: number;
  accountMaxPasswordLength: number;
}

@Component({
  selector: 'app-auth-form',
  templateUrl: 'auth-form.component.pug',
  styleUrls: [
    'auth-form.component.scss',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuthFormComponent implements OnInit, AfterViewInit, OnDestroy
{
  @ViewChild('username', { static: false }) usernameRef: ElementRef<HTMLInputElement>;

  private ngOnDestroy$: Subject<void> = new Subject<void>();

  public state: State = {
    form: this.fb.group({
      username: ['', [
        Validators.required,
        Validators.minLength(accountMinUsernameLength),
        Validators.maxLength(accountMaxUsernameLength),
      ]],
      password: ['', [
        Validators.required,
        Validators.minLength(accountMinPasswordLength),
        Validators.maxLength(accountMaxPasswordLength),
      ]],
    }),
    accountMinUsernameLength,
    accountMaxUsernameLength,
    accountMinPasswordLength,
    accountMaxPasswordLength,
  };

  constructor(
    private readonly fb: FormBuilder,
    private readonly matSnackbar: MatSnackBar,
    private readonly userCredentialsService: UserCredentialsService,
  ) {}

  ngOnInit(): void {
    (() => {
      this.userCredentialsService.userCredentials$.pipe(
        takeUntil(this.ngOnDestroy$),
      ).subscribe((userCredentials) => {
        if (userCredentials) {
          this.state.form.patchValue({
            username: userCredentials.username,
            password: userCredentials.password,
          } as FormValue);
        } else {
          this.state.form.reset();
        }
      });
    })();
  }

  ngAfterViewInit(): void {
    if (this.usernameRef && this.usernameRef.nativeElement) {
      this.usernameRef.nativeElement.focus();
    }
  }

  ngOnDestroy(): void {
    this.ngOnDestroy$.next();
  }

  get formValue(): FormValue {
    return this.state.form.value;
  }

  updateCredentials(): void {
    console.log(this.state.form);
    if (this.state.form.valid) {
      this.userCredentialsService.setUserCredentials({
        username: this.formValue.username,
        password: this.formValue.password,
      });

      this.matSnackbar.open('Credentials updated');
    } else {
      this.matSnackbar.open('Credentials cannot be updated');
    }

    if (this.usernameRef && this.usernameRef.nativeElement) {
      this.usernameRef.nativeElement.focus();
    }
  }
}
