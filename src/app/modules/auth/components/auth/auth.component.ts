import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  templateUrl: 'auth.component.pug',
  styleUrls: [
    'auth.component.scss',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuthComponent
{}
