import { Environment, EnvironmentConfiguration } from './_';

export const environment: EnvironmentConfiguration = {
  current: Environment.Production,
  enableAngularProductionMode: true,
  ttnApiEndpoint: undefined,
};
