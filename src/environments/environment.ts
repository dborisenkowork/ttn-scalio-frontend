import { EnvironmentConfiguration } from './_';

import { environment as localEnvironment } from './environment.local';

export const environment: EnvironmentConfiguration = localEnvironment;
