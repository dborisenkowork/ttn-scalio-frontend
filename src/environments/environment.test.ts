import { Environment, EnvironmentConfiguration } from './_';

export const environment: EnvironmentConfiguration = {
  current: Environment.Test,
  enableAngularProductionMode: false,
  ttnApiEndpoint: undefined,
};
