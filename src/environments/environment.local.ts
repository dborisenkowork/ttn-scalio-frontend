import { Environment, EnvironmentConfiguration } from './_';

export const environment: EnvironmentConfiguration = {
  current: Environment.Local,
  enableAngularProductionMode: true,
  ttnApiEndpoint: 'http://127.0.0.1:3000',
};

import 'zone.js/dist/zone-error';
