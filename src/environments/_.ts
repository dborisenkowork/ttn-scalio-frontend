export enum Environment {
  Local = 'local',
  Production = 'production',
  Test = 'test',
}

export interface EnvironmentConfiguration {
  current: Environment;
  enableAngularProductionMode: boolean;
  ttnApiEndpoint: string;
}
