# Test Assigment

## How to up project

Make sure your local backend is up: [https://gitlab.com/dborisenkowork/ttn-scalio-frontend]()

In case if you need a custom configuration, create your own configuration in `src/environments/environment.custom.ts`
and configure it in proper way

1. `npm install`
2. `npm run start` for using default environment configuration (`src/environment/enviroment.local.ts`) or
`npm run start:serve:custom` to use your custom environment configuration (`src/environments/environment.custom.ts`)
3. Open browser at [http://127.0.0.1:4200]()

## Developer notes

- Every commit is stable, feel free to navigate between them (there is a version with in-memory repository and a version 
with pgsql database)
- `commitlint` and `husky` packages are used to lint commit messages and running tests before committing anything
- Directory structure  is created manually since I don't like code scaffolding
