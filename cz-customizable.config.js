'use strict';

module.exports = {
  types: [
    {value: 'build',    name: 'build:    Changes that affect the build system or external dependencies (example scopes: package.json, scripts from developtools etc.)'},
    {value: 'ci',       name: 'ci:       Changes to our CI configuration files and scripts (example scopes: docker, docker-compose, .env etc.)'},
    {value: 'docs',     name: 'docs:     Documentation only changes'},
    {value: 'feat',     name: 'feat:     A new feature'},
    {value: 'fix',      name: 'fix:      A bug fix'},
    {value: 'perf',     name: 'perf:     A code change that improves performance'},
    {value: 'refactor', name: 'refactor: A code change that neither fixes a bug nor adds a feature'},
    {value: 'style',    name: 'style:    Changes that do not affect the meaning of the code\n            (white-space, formatting, missing semi-colons, etc)'},
    {value: 'test',     name: 'test:     Adding missing tests'},
    {value: 'revert',   name: 'revert:   Revert to a commit'},
    {value: 'merge',    name: 'merge:    Merge brunches' }
  ],
  messages: {
    type: 'Select the type of change that you\'re committing:',
    scope: '\nDenote the SCOPE of this change (optional):',
    customScope: 'Denote the SCOPE of this change:',
    subject: 'Write a SHORT, IMPERATIVE tense description of the change:\n',
    body: 'Provide a LONGER description of the change (optional). Use "|" to break new line:\n',
    breaking: 'List any BREAKING CHANGES (optional):\n',
    footer: 'List any ISSUES CLOSED by this change (optional). E.g.: #31, #34:\n',
    confirmCommit: 'Are you sure you want to proceed with the commit above?'
  },
  allowCustomScopes: true,
  allowBreakingChanges: ['feat', 'fix'],
  subjectLimit: 100
};
